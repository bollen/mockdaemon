Mock daemon features

--------------------

Mock daemon simulates a running software component.

On startup, the mock daemon enters a loop where it writes the following line to `/tmp/daemon.log`:

YYYY-MM-DD hh:mm:ss Mock daemon is still running


The line is written to the logfile in 30 second intervals by default.
Write interval can be set with the -i option

 

Watchdog features

-----------------

On startup, creates a log file with message: "Watchdog started" to `/tmp/watchdog.log`

The watchdog checks if the deamon is running every 10 seconds.
This interval can be changed using the -i parameter

If the deamon script is not running, it is restarted, and the message: 
"Daemon restarted" is written to `/tmp/watchdog.log`

The logfile size is checked every 5 minutes and rotated if over 1000 bytes.
The watchdog retains a set amount of old logfiles, can be changed in the params
