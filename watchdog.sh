#!/usr/bin/env sh                                                          

# Watchdog script, restarts daemon if not running and handles log rotation

#################################################
##
# author:   Mathias Andtbacka
# license:  MIT
# repo:     https://www.gitlab.com/bollen/mockdaemon
#
##
#################################################

# ------ vars ------
# CLI defailts
check_interval=5
rotate_interval=30
rotate_bytes=1000
logfile=/tmp/daemon.log
watchdog_logfile=/tmp/watchdog.log
daemon=$(dirname $0)/daemon.sh
keep_n_logs=1

# Usage function
usage(){
    echo "\
    Usage: $0 [OPTIONS]

    watchdog.sh makes sure daemon.sh is running and handles log rotation

        -i, --interval      Monitor interval in seconds
        -l, --logfile       Logfile to rotate
        -s, --script        Monitored scrpit
        -r, --rotate        Logrotate interval in seconds
        -b, --bytes         Logrotate if file size over N bytes
        -k, --keep          How many old logs to keep

    Example:
        $0 -i 5 -r 30 -b 1000 -l /tmp/daemon.log -s ./daemon.sh
    "
    exit 0
}

# ------ parse options ------

if [[ ! $# -eq 0 ]]; then 
    while [[ "$#" -gt 0 ]]; do
        case $1 in
            -i|--interval) check_interval="$2"; shift ;;
            -l|--logfile) logfile="$2"; shift ;;
            -s|--script) daemon="$2"; shift ;;
            -r|--rotate) rotate_interval="$2"; shift ;;
            -b|--bytes) rotate_bytes="$2"; shift ;;
            -k|--keep) keep_n_logs="$2"; shift ;;
            *) echo "Unknown parameter passed: $1"; exit 1 ;;
        esac
        shift
    done
fi

# ------ helpers ------
now(){
    local timestamp 
    if [[ $1 == "s" ]]; then
        timestamp=$(date +%s)
    else
        timestamp=$(date +'%F %H:%M:%S')
    fi
    echo $timestamp
}

# ------ monitoring ------
check_alive(){
    while true
    do
        if [[ ! $(pgrep -f $daemon) ]]; then
            $daemon
            echo >> $watchdog_logfile "$(now) Restarted daemon $!" 
        fi
        sleep $check_interval
    done
}


log_rotate(){
    while true
    do
        logfile_bytes=$(du -b $logfile | awk '{ print $1 }')
        if [[ $logfile_bytes -gt $rotate_bytes ]]; then
            echo >> $watchdog_logfile "$(now) rotating logs"
            # remove old logs first
            rm -r $(ls -t /tmp/daemon.log.* | sed -n $keep_n_logs',$p')
            mv $logfile $logfile.$(now s )
            touch $logfile
        fi
        sleep $rotate_interval
    done
}

# ------ main ------

main(){
    # create the watchdog logfile if not present
    [[ ! -f $watchdog_logfile ]] && >$watchdog_logfile
    echo "$(now) Watchdog started" >> $watchdog_logfile

    check_alive &
    log_rotate &
}


main 
