#!/usr/bin/env sh                                                          

# Mock daemon script to simulate a script logging to a file

#################################################
##
# author:   Mathias Andtbacka
# license:  MIT
# repo:     https://www.gitlab.com/bollen/mockdaemon
#
##
#################################################

# ------ vars ------
# CLI options

interval=30
logfile=/tmp/daemon.log

# ------ parse options ------

if ! [[ $# -eq 0 ]]; then 
    while [[ "$#" -gt 0 ]]; do
        case $1 in
            -i|--interval) interval="$2"; shift ;;
            -l|--logfile) logfile="$2"; shift ;;
            *) echo "Unknown parameter passed: $1"; usage && exit 1 ;;
        esac
        shift
    done
fi

# ------ check input ------ 

# create the logfile if it doesn't exist
[[ ! -f "$logfile" ]] && >$logfile


# ------ helpers ------

# Usage function
usage(){
    echo "\
    Usage: $0 [OPTIONS]

    daemon.sh is a script that simulates a program writing to a logfile

    optional args:
        -f, --file           Logfile to write to
        -i, --interval INT   Write interval in seconds
    
    Example:
    $0 -f /tmp/daemon.log -i 30
    "
    exit 0
}


# Timestamp
now(){
    local timestamp 
    if [[ $1 == "s" ]]; then
        timestamp=$(date +%s)
    else
        timestamp=$(date +'%F %H:%M:%S')
    fi
    echo $timestamp
}

# ------ main ------

main(){
    echo "$(now) Mock daemon stared, PID: $$" >> $logfile
    while true
    do
        echo "$(now) Mock daemon is still running" >> $logfile
        sleep $interval
    done

}


main & # start in background
